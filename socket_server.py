# coding: ISO-8859-1
import socket
import rsa_crypt

host = '127.0.0.1'
port = 80


def server_connection():
    # Opening server socket
    connection_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connection_client.bind((host, port))
    # Listening incoming connexion
    print("[Serveur] Le serveur ecoute...")
    connection_client.listen(0)

    # Endless execution loop
    while True:

        # Listening new connexion
        client, client_address = connection_client.accept()
        print("[Serveur] Connexion depuis " + str(client_address[0]) + ":" + str(client_address[1]))
        # Receiving a message from the client
        message = client.recv(1024)
        message = message.decode()
        # Reading encryption flag
        read_encryption = message[message.find("[") + len("["):message.rfind("]")]

        # Message is encrypted statement
        if read_encryption == "1":
            try:
                message = rsa_crypt.decrypt(message)
                print("[Serveur] Donnees encryptees recues : " + message)
            except:
                print("[Serveur] Decryption impossible, le message a ete altere !!!")
        # Message is in plain text statement
        else:
            message = message[message.find("(") + len("("):message.rfind(")")]
            print("[Serveur] Donnees non-encryptees recues : " + message)

    # Closing server socket (Unreachable)
    client.close()
    connection_client.close()
    print("[Serveur] Connexion fermee.")
