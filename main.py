
import data_generator
import socket_client
import time
import socket_server
import rsa_crypt

if __name__ == '__main__':

    # Getting user mode
    mode = input("1 Client / 2 Server")
    # Client mode statement
    if mode == "1":
        # Getting client encryption mode
        encryption = input("1 Encryption true / 2 Encryption false")
        # Endless execution loop
        while True:
            # Getting new data
            generated_data = data_generator.generate_data()
            # Encrypted messages statement
            if encryption == "1":
                print("[Client encrypte] Donnees generees : " + str(generated_data))
                # Encrypting data
                encrypted_generated_data = rsa_crypt.encrypt(generated_data)
                # Sending data
                socket_client.client_connection(encrypted_generated_data, 1)
            # Plaintext messages statement
            else:
                print("[Client non encrypte] Donnees generees : " + str(generated_data))
                # Sending data
                socket_client.client_connection(generated_data, 0)
            time.sleep(10)

    # Server mode statement
    else:
        socket_server.server_connection()
