import numpy


# Function returning random number between [0;1]
def generate_data():
    return numpy.random.rand(1)
