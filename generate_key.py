import rsa
from rsa import PublicKey

publicKey, privateKey = rsa.newkeys(512)

if __name__ == '__main__':

    with open('public.pem', 'wb+') as f:
        pk = publicKey.save_pkcs1()
        f.write(pk)
    with open('private.pem', 'wb+') as f:
        pk = privateKey.save_pkcs1()
        f.write(pk)
