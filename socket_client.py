import socket

target_url = 'http://127.0.0.1'

proxy_ip = '127.0.0.1'
proxy_port = 8080

CLRF = "\r\n"


def client_connection(generated_data, is_encrypted):
    # Connecting to proxy
    connection_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connection_server.connect((proxy_ip, proxy_port))

    # Printing, in client log, original data
    print("[Client] Donnees a envoyer : " + str(generated_data))

    # Initializing HTTP for targeted host
    http_request = (
            "POST " + target_url + " HTTP/1.1" + CLRF
            + "Host: " + target_url + CLRF
            + CLRF + "[" + str(is_encrypted) + "]" + CLRF
            + CLRF + "(" + str(generated_data) + ")" + CLRF
            + CLRF
    )

    # Sending request
    connection_server.send(http_request.encode('utf-8'))
    print("[Client] Donnees envoyees.")

    # Closing connexion
    connection_server.close()
    print("[Client] Connexion fermee.")
