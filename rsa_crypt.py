import base64

import rsa
from rsa import PublicKey, PrivateKey


def encrypt(generated_message):
    publicKey = load_public_key()

    encMessage = rsa.encrypt(str(generated_message).encode('utf-8'),
                             publicKey)
    # Encoding to base64
    encoded_string = base64.b64encode(encMessage)  # Bytes (base64)
    # Formatting string to send
    string_to_format = str(encoded_string)  # String (with bytes characters `b''`)
    string_to_send = string_to_format[2:len(string_to_format) - 1]  # String (w/ bytes characters)
    return string_to_send


def decrypt(encrypted_data):
    privateKey = load_private_key()

    encrypted_data = encrypted_data[encrypted_data.find("(") + len("("):encrypted_data.rfind(")")]
    # Retrieving base64 string
    retrieved_bytes = bytes(encrypted_data, 'utf-8')  # Bytes (base64)
    # Decoding base64 bytes
    decoded_bytes = base64.b64decode(retrieved_bytes)  # Bytes
    # Decrypting bytes with RSA private key
    retrieved_value = rsa.decrypt(decoded_bytes, privateKey).decode('utf-8')
    return retrieved_value


def load_private_key():
    with open('private.pem', 'rb') as f:
        pk = PrivateKey.load_pkcs1(f.read())
    return pk


def load_public_key():
    with open('public.pem', 'rb') as f:
        pk = PublicKey.load_pkcs1(f.read())
    return pk
