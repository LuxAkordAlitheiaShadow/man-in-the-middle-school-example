# Man in the Middle - Scholar example [FR]

## Introduction

Ce projet scolaire a pour but de démontrer l'impact des attaques de l'Homme du Milieu sur des données communiquantes.

### Cahier des charges

" Le but de ce système est d’éduquer et de sensibiliser les citoyens aux enjeux liés à la protection des
renseignements personnels et, entre autres, aux cyber menaces. 
Le système de visualisation pourra montrer la circulation des renseignements personnels et des données privées 
entre les objets connectés d’un utilisateur ou d’une maison sans aucune protection.

Dans ce projet, il fallait développer trois modules indépendants pour la procédure de collecte et
l’analyse des données qui circulent entre les objets connectés, la reconnaissance des cybermenaces 
en relation avec ces données non protégées et les objets qui peuvent contenir des
vulnérabilités de sécurité, et un système de visualisation pour montrer ces cybermenaces aux
utilisateurs. "

## Diagramme et technologies exploitees

Notre projet se présente avec 4 interfaces.

![Diagram](Ressources/Screenshots/Diagram.png)

### Les clients

Nous avons ici deux instances, appelées `client`, qui génèrent périodiquement des données de 0 à 1, pour le bien de la mise en pratique.

Ce qui différencie les deux clients est la qualité de chiffrement des messages avant la transmission de celles-ci.
Le client sans chiffrement va envoyer les données en `clair` tandis que le client avec chiffrement chiffre les données avec un `chiffrement asymétrique RSA` et un chiffrement en `base64` (pour permettre de manipuler la chaine côté serveur).

### Le proxy

Dans notre projet nous démontrons ce qu'il peut se produire sur une transmission de données avec une `attaque de l'Homme du Milieu` (Man in the Middle ou MitM en anglais).
Le principe de cette attaque est qu'un ordinateur va se placer entre la source et la destination d'un message pour l'intercepter, la visualiser, la modifier et la transmettre.

**Important :** Le but de la démonstration n'est pas de présenter la mise en place d'une telle attaque, qui impliquerait l'explication de notions telles que le routage réseau, l'empoisonnement ARP, etc., mais d'expliquer ce qui se peut se produire quand un Homme du Milieu est déjà présent.
De ce fait, nous ne mettons pas en place d'empoisonnement de la table ARP des clients, nous indiquons en conséquence au client de communiquer directement au proxy pour transmettre leurs messages.

Le proxy utilisé est l'outil proxy de BurpSuite en Community Edition.
Par défaut, ce dernier écoute les transmissions sur l'adresse et le port `127.0.0.1:8080`.

### Le serveur

L'instance qui collecte les données est appelée le `serveur`.
Il écoute toutes les transmissions sur l'adresse et le port `127.0.0.1:80`.

À la réception d'un message, le serveur va le déchiffrer si le message est chiffré, grâce à la clé RSA partagée (C.f. la mise en place du projet), avant d'afficher son contenu.
Si le message chiffré est altéré, le serveur sera dans l'incapacité de le déchiffrer et affichera un avertissement.

## Prerequis

### Python 3.9

Vous devez installer Python 3.9 sur votre machine.

https://www.python.org/downloads/

### BurpSuite

Vous devez installer BurpSuite Community Edition (la version gratuite de BurpSuite) sur votre machine.

https://portswigger.net/burp/releases/professional-community-2022-2-4?requestededition=community

## Mettre en place le projet

### Lancer le proxy

Lancer BurpSuite.

Créer simplement un projet temporaire, ici pas besoin de renseigner des champs.

![Launch_BurpSuite](Ressources/Screenshots/Launch_BurpSuite.png)
![Launch_BurpSuite_2](Ressources/Screenshots/Launch_BurpSuite_2.png)

Allez sur l'onglet `Proxy`.

![Toolsbar_BurpSuite](Ressources/Screenshots/Toolsbar_BurpSuite.png)

Vérifiez que l'intercepteur est actif, si cela n'est pas le cas cliquez simplement sur le bouton.

![Verify_Interceptor_BurpSuite](Ressources/Screenshots/Verify_Intercept_BurpSuite.png)

Le proxy est par défaut mis en place sur l'adresse et le port `127.0.0.1:8080`.

### Generer les cles RSA

Avant de lancer les diverses instances, vous devez lancer le générateur de clés RSA avec cette commande :

`./venv/bin/python generate_key.py`

Ce programme aura créé une paire de clés RSA dans le répertoire du projet.

### Lancer le serveur

Pour lancer l'instance du serveur, utilisez cette commande :

`./venv/bin/python main.py`

Puis entrer le mode `server` (`2`).

![Launch_Server](Ressources/Screenshots/Launch_Server.png)

Votre serveur est lancé.

### Lancer le client non encrypte

Pour lancer l'instance du client sans chiffrement, utilisez cette commande :

`./venv/bin/python main.py`

Puis entrer le mode `client` (`1`).

Et enfin entrer le mode `Encryption false` (`2`)

![Launch_Client_Unencrypted](Ressources/Screenshots/Launch_Client_Unencrypted.png)

Votre client sans chiffrement est lancé et envoie des données en clair au serveur via le proxy indéfiniment.

### Lancer le client encrypte

Pour lancer l'instance du client avec chiffrement, utilisez cette commande :

`./venv/bin/python main.py`

Puis entrer le mode `client` (`1`).

Et enfin entrer le mode `Encryption true` (`1`)

![Launch_Client_Encrypted](Ressources/Screenshots/Launch_Client_Encrypted.png)

Votre client avec chiffrement est lancé et envoie des données chiffrées au serveur via le proxy indéfiniment.

## Execution

### Transfert basique

Maintenant que le proxy ainsi que les diverses instances client/serveur sont en place, regardons le proxy.

Il a capturé un message de nos clients.
Ici vous pouvez analyser le contenu du message, le transférer jusqu'à son destinataire légitime (`Forward`), le détruire (`Drop`), le modifier (voir plus bas)...

![Plain_Proxy](Ressources/Screenshots/Plain_Proxy.png)

Si nous transferons le message le serveur reçoit bien les données.

![Plain_Server](Ressources/Screenshots/Plain_Server.png)

De même pour une donnée chiffrée.

![Encrypted_Proxy](Ressources/Screenshots/Encrypted_Proxy.png)
![Encrypted_Server](Ressources/Screenshots/Encrypted_Server.png)

### Transfert avec alteration

Maintenant que le proxy, et donc l'Homme du Milieu, peut voir et transférer les données, regardons comment les altérer.

Ici le client sans chiffrement génère une donnée.

![Send_Plain_Altered](Ressources/Screenshots/Send_Plain_Altered.png)

Dans le proxy on va modifier la valeur de la donnée en cliquant et altérant la valeur avant le transfert.

![Before_Plain_Altered](Ressources/Screenshots/Before_Plain_Altered.png)
![After_Plain_Altered](Ressources/Screenshots/After_Plain_Alterated.png)

Après le transfert, le serveur a bien la donnée altérée.

![Server_Plain_Alterated](Ressources/Screenshots/Server_Plain_Alterated.png)

On vient donc de démontrer l'impact d'une attaque de l'Homme du Milieu, entre sa capacité à visualiser, transférer, abandonner et altérer une donnée.

Maintenant regardons l'altération d'une donnée chiffrée.

![Send_Encrypted_Altered](Ressources/Screenshots/Send_Encrypted_Altered.png)

Dans un premier temps, l'Homme du milieu ne peut pas interpréter cette donnée à cause de son chiffrement.

![Before_Encrypted_Altered](Ressources/Screenshots/Before_Encrypted_Altered.png)

On peut toutefois altérer la donnée.

![After_Encrypted_Altered](Ressources/Screenshots/After_Encrypted_Alterated.png)

Cependant, après le transfert du message, le serveur n'arrive pas à déchiffrer la donnée altérée et donc indique qu'il y a un problème.

![Server_Encrypted_Altered](Ressources/Screenshots/Server_Encrypted_Alterated.png)

Cela démontre que le chiffrement de ses données permet, en plus de doter de confidentialité, de doter d'une protection d'intégrité de la donnée.
